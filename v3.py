import os
os.add_dll_directory(os.path.dirname(os.path.realpath(__file__)))

from pvss import Pvss
from pvss.ristretto_255 import create_ristretto_255_parameters
from pvss.ristretto_255 import Ristretto255Parameters

from asn1crypto.core import Asn1Value, Integer, OctetString
import asn1

from pvss.groups import ImageValue

from pvss.pvss import *
from pvss.ristretto_255 import *

import sys

SharedSecret._validate = lambda _: 0

num_of_auths = 0
num_of_voters = 0
security_thresh = 0

authorities = []
voters = []
params = []
auth_keys = []
receiver = None
params = None
rec_keys = ()

G = None
g = None


def set_schemes():
    global num_of_auths, num_of_voters, security_thresh
    num_of_auths = int(input("How many polling stations? "))
    num_of_voters = int(input("How many voters? "))
    security_thresh = int(input("Security threshold? "))


def initialize():
    global authorities, voters, receiver, auth_keys, params, G, g

    pvss_init = Pvss()
    params = create_ristretto_255_parameters(pvss_init) # (SystemParameters)

    g = pvss_init.params.g  # g0, g1  (ImageValue)
    G = pvss_init.params.G  # G0, G1
    # H = pvss_init.params.H

    # return

    # print(num_of_auths)
    for i in range(num_of_auths):
        name = "authority" + str(i)
        authorities += [Pvss()]
        authorities[i].set_params(params)  # (SystemParameters)
        priv, pub = authorities[i].create_user_keypair(name)  # (bytes, bytes)
        auth_keys += [(priv, pub)]
        
    for i in range(num_of_voters):
        voters += [Pvss()]
        voters[i].set_params(params)
        for k in auth_keys:
            voters[i].add_user_public_key(k[1])

    receiver = Pvss()
    receiver.set_params(params)
    priv, pub = receiver.create_user_keypair("receiver")
    for k in auth_keys:
        receiver.add_user_public_key(k[1])
    rec_keys = (priv, pub)


shares_by_auth = dict()
ballots = []
us = []
s =[]
ss = []

def vote(voter, choice):

    global ballots, shares_by_auth, us, s, ss 

    secret_b, shares_b = voter.share_secret(security_thresh)

    secret = Secret.from_der(voter, secret_b)

    shared_secret = SharedSecret.from_der(voter, shares_b)
    ss += [shared_secret]
    shares = shared_secret.shares

    i = 0
    for share in shares:
        if i in shares_by_auth:
            saved_shares = shares_by_auth[i]
            saved_shares += [share]
        else:
            saved_shares = [share]
        shares_by_auth[i] = saved_shares
        i += 1

    # u = G^(s+v) = G^s * G^v

    g_s = secret.secret     # RistrettoPoint
    s += [g_s]

    if choice == 1:
        u = G[0].__mul__(g_s)
    else:
        u = g_s

    us += [u]    

    proof = construct_proof()

    ballot = (u, shares, proof)
    ballots += [ballot]
    

def bulletin_board():
    print("\n----- OGLASNA PLOČA -----\n")

    for i in range(len(ballots)):
        ballot = ballots[i]
        print("Ballot no. " + str(i+1))
        print(ballot[0])
        print(ballot[1])
        print(ballot[2])
        print()

    print("-------------------------\n")


def construct_proof():
    return True


def voting():
    for voter in voters:
        choice = int(input("Yes/No? [1/0] "))
        vote(voter, choice)
    bulletin_board()    
    return


def tally():

    receivers_shares = []  # List[Share]
    
    for i, a in enumerate(authorities, 0):

        priv = auth_keys[i][0]
        priv = PrivateKey.from_der(a, priv)
        shares_for_auth = shares_by_auth[i]  # Share

        dec_shares = [share.share ** priv.priv.inv for share in shares_for_auth] # Ristretto255Point
        mul_shares = prod(dec_shares)

        new_share = Share.create(pvss=a, pub_name="authority" + str(i), share=mul_shares, resp=shares_for_auth[0].resp)      
        
        receivers_shares += [new_share]

    receiver = Pvss()
    receiver.set_params(params)
    rec_priv, rec_pub = receiver.create_receiver_keypair("receiver")

    receivers_secret = SharedSecret.create(pvss=receiver, shares=receivers_shares, coeffs=ss[0].coefficients, challenge=b"0")
    receiver.set_shares(receivers_secret.der)

    # reenkripcija

    for keys in auth_keys:
        receiver.add_user_public_key(keys[1])
    
    for i, authority in enumerate(authorities):
        # a_name = "renew" + str(i)
        p = Pvss()
        p.set_params(params)
        for keys in auth_keys:
            p.add_user_public_key(keys[1])
        p.set_shares(receivers_secret.der)
        p.set_receiver_public_key(rec_pub)
        reenc = p.reencrypt_share(auth_keys[i][0])
        receiver.add_reencrypted_share(reenc)

    secret = receiver.reconstruct_secret(rec_priv)

    secrets = prod(s)

    secret_point = Secret.from_der(pvss=receiver, data=secret).secret
    
    if (secret_point == secrets):
        print("Sucess!")
    else:
        print("Fail.")

    
    

    # mul_s = prod(receivers_shares) # ovo je problem jer se ne mnoze samo nego se trebaju properly rekonstruirat?
    
    '''mul_u = prod(us)
    
    mul_v = mul_u.__floordiv__(mul_s)

    print("Tallying completed.\n")

    for i in range(0, len(voters)+1):
        
        if i == 0:
            g_i = 1
            # ?
        else:
            g_i = G[0].__pow__(i)

        print(g_i.__eq__(mul_v))

        if g_i.__eq__(mul_v):
            finalize(i)'''
    

def multiply_shares(pvss, s1, s2):
    assert(s1.pub_name == s2.pub_name)
    return Share.create(pvss=pvss, pub_name=s1.pub_name, share=s1.share*s2.share, resp=s1.resp)
    
def multiply_shared_secrets(pvss, s1, s2):
    shares1 = {s.pub_name:s for s in s1.shares}
    shares2 = {s.pub_name:s for s in s2.shares}
    assert(shares1.keys() == shares2.keys())
    new_shares = [multiply_shares(pvss, shares1[name], shares2[name]) for name in shares1.keys()]
    return SharedSecret.create(pvss=pvss, shares=new_shares, coeffs=s1.coefficients, challenge=b"0")


def finalize(total):
    if total > len(voters)/2:
        print("\nTotal vote: YES")
    else:
        print("\nTotal vote: NO")


set_schemes()
initialize()

voting()
tally()
finalize(15)





    
