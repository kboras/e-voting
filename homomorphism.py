#!/usr/bin/env python3

import os
os.add_dll_directory(os.path.dirname(os.path.realpath(__file__)))

from pvss import Pvss
from pvss.pvss import SharedSecret
from pvss.pvss import Share
from pvss.pvss import Secret
from pvss.ristretto_255 import create_ristretto_255_parameters

pvss_init = Pvss()
params = create_ristretto_255_parameters(pvss_init)

# Disable validation of shared secrets
SharedSecret._validate = lambda _: 0

def multiply_shares(pvss, s1, s2):
    """Multiply two shares"""
    # Shares need to belong to the same user
    assert(s1.pub_name == s2.pub_name)
    # Multiply the group elements, we cannot compute resp so we just use resp 
    # from the first share which is invalid for the new share now. This will not
    # validate but we don't care
    return Share.create(pvss=pvss, pub_name=s1.pub_name, share=s1.share*s2.share, resp=s1.resp)

def multiply_shared_secrets(pvss, s1, s2):
    """Given two SharedSecrets -- each containing shares for the same group of users, multiply coresponding shares."""
    shares1 = {s.pub_name:s for s in s1.shares}
    shares2 = {s.pub_name:s for s in s2.shares}
    # Users need to be same in both Objects
    assert(shares1.keys() == shares2.keys())
    # Calculate new shares
    new_shares = [multiply_shares(pvss, shares1[name], shares2[name]) for name in shares1.keys()]
    # from the first share which is invalid for the new share now.
    # We need to stuff new shares into a SharedSecret object, again we don't have the correct
    # coefficients for validation but we don't care.
    return SharedSecret.create(pvss=pvss, shares=new_shares, coeffs=s1.coefficients, challenge=b"0")

def test_homomorphism():
    # Generate 3 players
    player_names = [
        "player1",
        "player2",
        "player3"
    ]
    player_keys = dict()
    for name in player_names:
        p = Pvss()
        p.set_params(params)
        priv, pub = p.create_user_keypair(name)
        player_keys[name] = (priv, pub)

    # Generate 2 secrets
    secrets = []
    shares = []
    for _ in range(2):
        pvss_dealer = Pvss()
        pvss_dealer.set_params(params)
        for name, (_, pub) in player_keys.items():
            pvss_dealer.add_user_public_key(pub)
        secret, _ = pvss_dealer.share_secret(2)
        secrets.append(secret)
        shares.append(pvss_dealer.shares)

    # For each player, multiply the corresponding shares from two secrets
    pvss_receiver = Pvss()
    pvss_receiver.set_params(params)
    
    for name, (_, pub) in player_keys.items():
        pvss_receiver.add_user_public_key(pub)
    new_shares = multiply_shared_secrets(pvss_receiver, shares[0], shares[1])
    pvss_receiver.set_shares(new_shares.der)
 
    # Try to reconstruct the secret from the new shares.
    # Pvss API doesn't allow us just to recrypt
    # the shares and reconstruct the recret -- we need to reencrypt them with the receiver's
    # key and then decrypt and reconstruct.
    recv_priv, recv_pub = pvss_receiver.create_receiver_keypair("receiver")
    for player in ["player1", "player2"]:
        p = Pvss()
        p.set_params(params)
        for name, (_, pub) in player_keys.items():
            p.add_user_public_key(pub)
        # Hack 
        p.set_shares(new_shares.der)
        p.set_receiver_public_key(recv_pub)
        reenc = p.reencrypt_share(player_keys[player][0])
        pvss_receiver.add_reencrypted_share(reenc)

    secret = pvss_receiver.reconstruct_secret(recv_priv)
    secret_point = Secret.from_der(pvss=pvss_receiver, data=secret).secret
    secret0_point = Secret.from_der(pvss=pvss_receiver, data=secrets[0]).secret
    secret1_point = Secret.from_der(pvss=pvss_receiver, data=secrets[1]).secret
    if (secret_point == secret0_point*secret1_point):
        print("Sucess!")
    else:
        print("Fail.")

test_homomorphism()
