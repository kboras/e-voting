import os
os.add_dll_directory(os.path.dirname(os.path.realpath(__file__)))

from pvss import Pvss
from pvss.ristretto_255 import create_ristretto_255_parameters
from pvss.ristretto_255 import Ristretto255Parameters

from asn1crypto.core import Asn1Value, Integer, OctetString
import asn1

from pvss.groups import ImageValue

from pvss.pvss import *
from pvss.ristretto_255 import *

import sys

num_of_auths = 0
num_of_voters = 0
security_thresh = 0

authorities = []
voters = []
params = []
auth_keys = []
receiver = None
params = None
rec_keys = ()

G = None
g = None


def set_schemes():
    global num_of_auths, num_of_voters, security_thresh
    num_of_auths = int(input("How many polling stations? "))
    num_of_voters = int(input("How many voters? "))
    security_thresh = int(input("Security threshold? "))


def initialize():
    global authorities, voters, receiver, auth_keys, params, G, g

    pvss_init = Pvss()
    params = create_ristretto_255_parameters(pvss_init) # (SystemParameters)

    g = pvss_init.params.g  # g0, g1  (ImageValue)
    G = pvss_init.params.G  # G0, G1
    H = pvss_init.params.H

    return

    # print(num_of_auths)
    for i in range(num_of_auths):
        name = "authority" + str(i)
        authorities += [Pvss()]
        authorities[i].set_params(params)  # (SystemParameters)
        priv, pub = authorities[i].create_user_keypair(name)  # (bytes, bytes)
        auth_keys += [(priv, pub)]
        
    for i in range(num_of_voters):
        voters += [Pvss()]
        voters[i].set_params(params)
        for k in auth_keys:
            voters[i].add_user_public_key(k[1])

    receiver = Pvss()
    receiver.set_params(params)
    priv, pub = receiver.create_user_keypair("receiver")
    for k in auth_keys:
        receiver.add_user_public_key(k[1])
    rec_keys = (priv, pub)


shares_by_auth = dict()
ballots = []
us = []

def vote(voter, choice):

    global ballots, shares_by_auth, us

    secret_b, shares_b = voter.share_secret(security_thresh)

    secret = Secret.from_der(voter, secret_b)

    shared_secret = SharedSecret.from_der(voter, shares_b)
    shares = shared_secret.shares

    i = 0
    for share in shares:        
        if i in shares_by_auth:
            saved_shares = shares_by_auth[i]
            saved_shares += [share]
        else:
            saved_shares = [share]
        shares_by_auth[i] = saved_shares
        i += 1

    # u = G^(s+v) = G^s * G^v

    g_s = secret.secret     # RistrettoPoint

    if choice == 1:
        u = G[0].__mul__(g_s)
    else:
        u = g_s

    us += [u]    

    proof = construct_proof()

    ballot = (u, shares, proof)
    ballots += [ballot]
    

def bulletin_board():
    print("\n----- OGLASNA PLOČA -----\n")

    for i in range(len(ballots)):
        ballot = ballots[i]
        print("Ballot no. " + str(i+1))
        print(ballot[0])
        print(ballot[1])
        print(ballot[2])
        print()

    print("-------------------------\n")


def construct_proof():
    return True


def voting():
    for voter in voters:
        choice = int(input("Yes/No? [1/0] "))
        vote(voter, choice)
    bulletin_board()    
    return


def tally():

    receivers_shares = []  # Ristretto255Points
    
    for i, a in enumerate(authorities, 0):

        shares_for_auth = shares_by_auth[i]

        # ss = SharedSecret.create(a, shares_for_auth, [], Challenge().der)

        # reenc_shares_for_auth = [ReencryptedShare.reencrypt(a, auth_keys[i][0]) for share in shares_for_auth]

        imvals = [share.share for share in shares_for_auth]

        mul_shares = prod(imvals)

        receivers_shares += [mul_shares]

        # print(mul_shares)

    mul_s = prod(receivers_shares)

    mul_u = prod(us)

    '''print(type(mul_s))
    print(type(mul_u))
    print(type(mul_shares))'''

    mul_v = mul_u.__floordiv__(mul_shares)

    # print(type(mul_v))

    # print(len(voters))

    print("Tallying completed.\n")

    for i in range(1, len(voters)+1):
        g_i = G[0].__pow__(i)

        # print(g_i)
        # print(mul_v)

        print(g_i.__eq__(mul_v))

        if g_i.__eq__(mul_v):
            finalize(i)
    


def finalize(total):
    if total > len(voters)/2:
        print("\nTotal vote: YES")
    else:
        print("\nTotal vote: NO")


set_schemes()
initialize()

# voting()
# tally()





    
