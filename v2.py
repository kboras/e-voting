import os
os.add_dll_directory(os.path.dirname(os.path.realpath(__file__)))

from pvss import Pvss
from pvss.ristretto_255 import create_ristretto_255_parameters
from pvss.ristretto_255 import Ristretto255Parameters

from asn1crypto.core import Asn1Value, Integer, OctetString
import asn1

from pvss.groups import ImageValue

from pvss.pvss import *
from pvss.ristretto_255 import *

import sys

SharedSecret._validate = lambda _: 0

num_of_auths = 0
num_of_voters = 0
security_thresh = 0

authorities = dict()
voters = []
params = []
auth_keys = dict()
receiver = None
params = None
rec_keys = ()

G = None
g = None


def set_schemes():
    global num_of_auths, num_of_voters, security_thresh
    num_of_auths = int(input("How many polling stations? "))
    num_of_voters = int(input("How many voters? "))
    security_thresh = int(input("Security threshold? "))


def initialize():
    global authorities, voters, receiver, auth_keys, params, G, g

    pvss_init = Pvss()
    params = create_ristretto_255_parameters(pvss_init) # (SystemParameters)

    g = pvss_init.params.g  # g0, g1  (ImageValue)
    G = pvss_init.params.G  # G0, G1
    # H = pvss_init.params.H

    # return

    # generate authorities
    for i in range(num_of_auths):
        name = "authority" + str(i)
        authorities[name] = Pvss()
        authorities[name].set_params(params)  # (SystemParameters)
        priv, pub = authorities[name].create_user_keypair(name)  # (bytes, bytes)
        auth_keys[name] = (priv, pub)

    # generate voters
    for i in range(num_of_voters):
        voters += [Pvss()]
        voters[i].set_params(params)
        for k in auth_keys:
            voters[i].add_user_public_key(auth_keys[k][1])

    receiver = Pvss()
    receiver.set_params(params)
    priv, pub = receiver.create_user_keypair("receiver")
    for k in auth_keys:
        receiver.add_user_public_key(auth_keys[k][1])
    rec_keys = (priv, pub)


shares_by_auth = dict()
ballots = []
us = []
s =[]

shared_secrets = []
shares = []

def vote(voter, choice):

    global ballots, shares_by_auth, us, s, shared_secrets, shares

    secret_b, shares_b = voter.share_secret(security_thresh)

    secret = Secret.from_der(voter, secret_b)

    shared_secret = SharedSecret.from_der(voter, shares_b)
    shares = shared_secret.shares

    shared_secrets += [shared_secret]
    shares += [voter.shares]
    
    # proof = construct_proof()

    # ballot = (u, shares, proof)
    # ballots += [ballot]
    

def bulletin_board():
    print("\n----- OGLASNA PLOČA -----\n")

    for i in range(len(ballots)):
        ballot = ballots[i]
        print("Ballot no. " + str(i+1))
        print(ballot[0])
        print(ballot[1])
        print(ballot[2])
        print()

    print("-------------------------\n")


def construct_proof():
    return True


def voting():
    for voter in voters:
        choice = int(input("Yes/No? [1/0] "))
        vote(voter, choice)
    bulletin_board()    
    return


def tally():

    receiver = Pvss()
    receiver.set_params(params)
    rec_shares = []

    for a in auth_keys:
        receiver.add_user_public_key(auth_keys[a][1])

    new_shares = multiply_shared_secrets(receiver, shares)
    receiver.set_shares(new_shares.der)
        

        

    talliers_ss = SharedSecret.create(pvss=tallier, shares=talliers_shares, coeffs=s[0].coefficients, challenge=b"0")

    tallier.set_shares(talliers_ss.der)

    tal_priv, tal_pub = tallier.create_receiver_keypair("tallier")

    for authority in ["authority0", "authority1"]:
        a = Pvss()
        a.set_params(params)
        for name, (priv, pub) in auth_keys.items():
            tallier.add_user_public_key(pub)

        a.set_shares(talliers_ss.der) # ?
        a.set_receiver_public_key(tal_pub)
        reenc = a.reencrypt_share(auth_keys[authority][0])
        tallier.add_reencrypted_share(reenc)

    secret = tallier.reconstruct_secret(tal_priv)
    secret_points = [Secret.from_der(pvss=tallier, data=i).secret for i in s]
    secret_point = Secret.from_der(pvss=tallier, data=secret).secret

    if (secret_point == prod(secret_points)):
        print("Sucess!")
    else:
        print("Fail.")


def multiply_shares(pvss, s1, s2):
    """Multiply two shares"""
    assert(s1.pub_name == s2.pub_name)
    return Share.create(pvss=pvss, pub_name=s1.pub_name, share=s1.share*s2.share, resp=s1.resp)

def multiply_shared_secrets(pvss, s1, s2):
    """Given two SharedSecrets -- each containing shares for the same group of users, multiply coresponding shares."""
    new_shares = []
    for s in shares:
        
    shares1 = {s.pub_name:s for s in s1.shares}
    shares2 = {s.pub_name:s for s in s2.shares}
    assert(shares1.keys() == shares2.keys())
    new_shares = [multiply_shares(pvss, shares1[name], shares2[name]) for name in shares1.keys()]
    return SharedSecret.create(pvss=pvss, shares=new_shares, coeffs=s1.coefficients, challenge=b"0")


def finalize(total):
    if total > len(voters)/2:
        print("\nTotal vote: YES")
    else:
        print("\nTotal vote: NO")


set_schemes()
initialize()

voting()
tally()





    
