import os
os.add_dll_directory(os.path.dirname(os.path.realpath(__file__)))

from pvss import Pvss
from pvss.ristretto_255 import create_ristretto_255_parameters
from pvss.pvss import *

SharedSecret._validate = lambda _: 0


def set_schemes():
    global num_of_auths, num_of_voters, security_thresh
    num_of_auths = int(input("How many polling stations? "))
    num_of_voters = int(input("How many voters? "))
    security_thresh = int(input("Security threshold? "))


def initialize():
    global authorities, voters, receiver, auth_keys, params, G, g
    pvss_init = Pvss()
    params = create_ristretto_255_parameters(pvss_init) # SystemParameters
    g = pvss_init.params.g  # g0, g1  Ristretto255Point
    G = pvss_init.params.G  # G0, G1
    receiver = Pvss()
    receiver.set_params(params)

    for i in range(num_of_auths):
        name = "authority" + str(i)
        authorities += [Pvss()]
        authorities[i].set_params(params)  # SystemParameters
        priv, pub = authorities[i].create_user_keypair(name)  # bytes, bytes
        auth_keys[name] = (priv, pub)
        
    for i in range(num_of_voters):
        voters += [Pvss()]
        voters[i].set_params(params)
        for k in auth_keys.values():
            voters[i].add_user_public_key(k[1])


def vote(voter, choice):
    global ballots, shares_by_auth, us, secrets, shared_secrets
    secret_b, shares_b = voter.share_secret(security_thresh)
    secret = Secret.from_der(voter, secret_b)  # Secret
    shared_secret = SharedSecret.from_der(voter, shares_b)  # SharedSecret
    shared_secrets += [shared_secret]
    shares = shared_secret.shares   # List[Share]

    i = 0
    for share in shares:
        if i in shares_by_auth:
            saved_shares = shares_by_auth[i]
            saved_shares += [share]
        else:
            saved_shares = [share]
        shares_by_auth[i] = saved_shares
        i += 1

    # u = G^(s+v) = G^s * G^v
    g_s = secret.secret
    secrets += [g_s]
    if choice == 1:
        u = G[0].__mul__(g_s)
    else:
        u = g_s
    us += [u]
    c0 = 0
    proof = construct_proof(u, c0)
    ballot = (u, shares, proof)
    ballots += [ballot]
    

def bulletin_board():
    print("\n----- BULLETIN BOARD -----\n")
    for i in range(len(ballots)):
        ballot = ballots[i]
        print("Ballot no. " + str(i+1))
        print(ballot[0])
        print(ballot[1])
        print(ballot[2])
        print()
    print("----------------------------\n")


def construct_proof(u, c0):
    return True


def voting():
    for voter in voters:
        choice = int(input("Yes/No? [1/0] "))
        vote(voter, choice)
    bulletin_board()    
    return


def tally():
    receivers_shares = []  # List[Share]
    rec_priv, rec_pub = receiver.create_receiver_keypair("receiver")
    
    for i, a in enumerate(authorities, 0):
        shares_for_auth = shares_by_auth[i]  # Share
        dec_shares = [share.share for share in shares_for_auth] # Ristretto255Point
        mul_shares = prod(dec_shares)
        new_share = Share.create(pvss=a, pub_name="authority" + str(i), share=mul_shares, resp=shares_for_auth[0].resp)      
        receivers_shares += [new_share]

    receivers_secret = SharedSecret.create(pvss=receiver, shares=receivers_shares, coeffs=shared_secrets[0].coefficients, challenge=b"0")
    receiver.set_shares(receivers_secret.der)

    for keys in auth_keys.values():
        receiver.add_user_public_key(keys[1])
    
    # for i, authority in enumerate(auth_keys):
    for i in range(security_thresh):
        authority = list(auth_keys.keys())[i]
        p = Pvss()
        p.set_params(params)
        for keys in auth_keys.values():
            p.add_user_public_key(keys[1])
        p.set_shares(receivers_secret.der)
        p.set_receiver_public_key(rec_pub)
        reenc = p.reencrypt_share(auth_keys[authority][0])
        receiver.add_reencrypted_share(reenc)

    secret = receiver.reconstruct_secret(rec_priv)
    secrets_mul = prod(secrets)
    secret_point = Secret.from_der(pvss=receiver, data=secret).secret
    
    if (secret_point == secrets_mul):
        print("Sucess!")
    else:
        print("Fail.")

    mul_v = prod(us).__floordiv__(secret_point)

    for i in range(1, len(voters)+1):
        g_i = G[0].__pow__(i)
        if g_i.__eq__(mul_v):
            finalize(i)


def finalize(total):
    if total > len(voters)/2:
        print("\nTotal vote: YES")
    else:
        print("\nTotal vote: NO")


if __name__ == "__main__":

    authorities = []
    voters = []
    auth_keys = dict()

    shared_secrets = []
    secrets = []
    shares_by_auth = dict()
    us = []
    ballots = []
    
    set_schemes()
    initialize()
    voting()
    tally()
